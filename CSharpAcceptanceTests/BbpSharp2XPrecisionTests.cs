
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CSharpAcceptanceTests
{
    [TestClass]
    public class BbpSharp2XPrecisionTests : BbpTestsBase
    {
        protected override Func<int, (double, string)> WorkerDelegate => BbpSharp.BbpSharp2XPrecision.CalcPi;
        protected override int ValuableDigits { get; set; } = 17;

        [TestMethod]
        public void BbpSharp2XPrecisionTest100000_100()
        {
            var hex = CalcBppHex(100_000, 100);
            Assert.AreEqual(Resources.Pi100000_100_hex, hex);
        }

        [TestMethod]
        public void BbpSharp2XPrecisionTest1000000_14()
        {
            var hex = CalcBppHex(1_000_000, 14);
            Assert.AreEqual(Resources.Pi1000000_14_hex, hex);
        }
    }
}
