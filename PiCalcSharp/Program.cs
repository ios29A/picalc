﻿using System;
using System.Diagnostics;

namespace PiCalcSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            var digits = args.Length == 0 ? 1000 : int.Parse(args[0]);

            Stopwatch watch = Stopwatch.StartNew();
            var result = PiCalc.GetDigits(digits);
            watch.Stop();

            Console.WriteLine($"Pi with {digits}");
            Console.WriteLine(result);
            Console.WriteLine($"Duration: {watch.Elapsed.TotalSeconds}s");
        }
    }
}
