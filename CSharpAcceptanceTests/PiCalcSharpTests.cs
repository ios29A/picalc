﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PiCalcSharp;

namespace CSharpAcceptanceTests
{
    [TestClass]
    public class PiCalcSharpTests
    {
        [TestMethod]
        public void PiCalcSharp10000()
        {
            Assert.AreEqual(Resources.Pi10000_dec, PiCalc.GetDigits(10000));
        }
    }
}
