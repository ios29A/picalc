#include "pch.h"
#include "CppUnitTest.h"
#include "..\BbpCpp\BbpCpp.h"
#include <vector>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CppAcceptanceTests
{
	TEST_CLASS(BbpCppTests)
	{
		const int valuableDigits = 10;
		const std::string Pi100000_100_hex = "35EA16C406363A30BF0B2E693992B58F7205A7232C4168840B6A48ECB67EAA2A5B9D3C96CD9BCD70C2A1B0FB41A5F475A4A5";
	protected:
		std::string* bbp_13_not_precise(int position)
		{
			PRECISION piArr;
			auto rv = std::vector<char>(13, '0');
			BbpfCalc(&piArr, &position);
			ToHex(&rv[0], &piArr);
			return new std::string(&rv[0], 13);
		}

		std::string Bbp(int position, int count)
		{
			int batchSize = (int) ceil((double)count / valuableDigits);
			std::string finalString;
			for (int batch = 0; batch < batchSize; batch++)
			{
				auto hex = bbp_13_not_precise(position + batch * valuableDigits);
				finalString += (*hex).substr(0, valuableDigits);
				delete hex;
			}
			finalString = finalString.substr(0, count);
			return finalString;
		}
	public:

		TEST_METHOD(BbpCppTest100000_100)
		{
			auto value = Bbp(100000, 100);
			Assert::AreEqual(Pi100000_100_hex, value);
		}
	};
}
