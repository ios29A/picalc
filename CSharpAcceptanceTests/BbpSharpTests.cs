using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CSharpAcceptanceTests
{
    [TestClass]
    public class BbpSharpTests : BbpTestsBase
    {
        protected override Func<int, (double, string)> WorkerDelegate => BbpSharp.BbpSharp.CalcPi;
        protected override int ValuableDigits { get; set; } = 10;

        [TestMethod]
        public void BbpSharpTest100000_100()
        {
            var hex = CalcBppHex(100_000, 100);
            Assert.AreEqual(Resources.Pi100000_100_hex, hex);
        }

        [TestMethod]
        public void BbpSharpTest1000000_14()
        {
            var hex = CalcBppHex(1_000_000, 14);
            Assert.AreEqual(Resources.Pi1000000_14_hex, hex);
        }
    }
}
