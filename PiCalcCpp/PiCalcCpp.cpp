// PiCalcsCpp.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <chrono>
#include <ctime>


char* CalcSingleCorePi(int digits)
{
	digits++;

	int xLength = digits * 10 / 3 + 2;
	uint32_t* x = new uint32_t[xLength];
	uint32_t* r = new uint32_t[xLength];

	uint32_t* pi = new uint32_t[digits];

	for (int j = 0; j < xLength; j++)
		x[j] = 20;

	for (int i = 0; i < digits; i++)
	{
		uint32_t carry = 0;
		for (int j = 0; j < xLength; j++)
		{
			uint32_t num = (uint32_t)(xLength - j - 1);
			uint32_t dem = num * 2 + 1;

			x[j] += carry;

			uint32_t q = x[j] / dem;
			r[j] = x[j] % dem;

			carry = q * num;
		}

		pi[i] = x[xLength - 1] / 10;

		r[xLength - 1] = x[xLength - 1] % 10; ;

		for (int j = 0; j < xLength; j++)
			x[j] = r[j] * 10;
	}
	char* result = new char[digits + 1];
	result[digits] = '\0';
	uint32_t c = 0;
	for (int i = digits - 1; i >= 0; i--)
	{
		pi[i] += c;
		c = pi[i] / 10;
		char d = (char)((pi[i] % 10) + '0');
		result[i] = d;
	}
	delete x;
	delete r;
	delete pi;
	return result;
};

int main(int argc, char* argv[])
{
	int digits = 1000;
	if (argc > 1)
	{
		digits = atoi(argv[1]);
	}
	auto start = std::chrono::system_clock::now();
	char* result = CalcSingleCorePi(digits);
	auto end = std::chrono::system_clock::now();

	std::cout << "Pi with " << digits << " digits:" << std::endl;
	std::cout << result << std::endl;
	std::cout << "Duration: " << (std::chrono::duration<double> (end - start)).count() << "s" << std::endl;
	delete result;
}


