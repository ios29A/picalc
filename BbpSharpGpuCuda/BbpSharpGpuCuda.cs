﻿using ILGPU;
using ILGPU.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BbpSharp
{
    public class BbpSharpGpuCuda
    {
        private static double ExpoMod(int n, double k)
        {
            int bitsneeded = 0;//The number of binary positions and the location in the table of the highest needed power
            for (; (1 << bitsneeded) < n; bitsneeded++) ;//Move through table to find position of the largest power of two < n
            if ((1 << bitsneeded) != n) //because the increment is applied before the condition check, we need to decrement by one, unless the item was equal to n
            {
                bitsneeded--;
            }
            //First set t to be the largest power of two such that t ≤ n, and set r = 1.
            //int t = powerTable[bitsneeded];
            int t = 1 << bitsneeded;
            double r = 1;

            //Loop by the number of Binary positions
            for (int i = 0; i <= bitsneeded; i++)
            {
                if (n >= t) // if n ≥ t then
                {
                    r = r * 16.0;// r ← br
                    r = r - ((int)(r / k)) * k;// r ← r mod k
                    n = n - t;// n ← n − t
                }
                t = t / 2;// t ← t/2
                if (t >= 1) // if t ≥ 1 then
                {
                    r = r * r;// r ← r^2
                    r = r - ((int)(r / k)) * k;// r ← r mod k
                }
            }
            return r;
        }

        static void LeftPortionThreaded(
            Index1 index,              // The global thread index (1D in this case)
            ArrayView<int> kPointer,
            ArrayView<int> jPointer,
            ArrayView<int> dPointer,
            ArrayView<double> resView)//,   // A view to a chunk of memory (1D in this case)
        {
            int i = index;
            int k = kPointer[i], j = jPointer[i], d = dPointer[i];
            int kInit = k;
            int termsNo = 0;//Always go from 0-100000 in the terms[] DONT use K!
            double s = 0;
            for (; k < (kInit + 100000); k++)
            {
                double denominator;
                denominator = 8.0 * k + j;
                var numerator = ExpoMod(d - k, denominator);
                s += numerator / denominator;
                s -= (int)s;
                termsNo++;
            }
            resView[index] = s;
        }
        private struct GPUTask
        {
            public int k;
            public int j;
            public int d;
        }

        //Bailey–Borwein–Plouffe Formula 16^d x Sj
        private static double Bbpf16jsd(Accelerator accelerator, Action<Index1, ArrayView<int>, ArrayView<int>, ArrayView<int>, ArrayView<double>> kernel, int j, int d)
        {
            double s = 0;
            double numerator, denominator;
            double term;
            int noOfThreads = 2048;// accelerator.MaxNumThreads;
            var listOfGPUTasks = new List<GPUTask>(noOfThreads);
            int k = 0;
            while (k < d)
            {
                if (k + (noOfThreads) < d)//Only make threads for k up to less than d
                {
                    listOfGPUTasks.Clear();
                    for (int i1 = 0; i1 < noOfThreads; i1++) //create and execute (noOfThreads) threads
                    {
                        listOfGPUTasks.Add(new GPUTask() { k = k, j = j, d = d });
                        k = k + 100000; //We need to run 100000 result on each thread because the thread overhead is much to great to run just 1
                    }

                    var kArray = accelerator.Allocate(listOfGPUTasks.Select(t => t.k).ToArray());
                    var jArray = accelerator.Allocate(listOfGPUTasks.Select(t => t.j).ToArray());
                    var dArray = accelerator.Allocate(listOfGPUTasks.Select(t => t.d).ToArray());
                    var resArray = accelerator.Allocate<double>(listOfGPUTasks.Count);

                    kernel(kArray.Length, kArray, jArray, dArray, resArray);
                    accelerator.Synchronize();

                    var res = resArray.GetAsArray();
                    for (int i2 = noOfThreads - 1; i2 > -1; i2--) //fetch results from all threads - count backwards because last thread executed will be slowest (higher numbers)
                    {
                        s = s + res[i2];
                        s = s - (int)s;
                    }
                }
                else //if we are almost done and k + noOfThreads > d then do the last few terms single threaded
                {
                    denominator = 8d * k + j;
                    numerator = ExpoMod(d - k, denominator); // Binary algorithm for exponentiation with Modulo must be used becuase otherwise 16^(d-k) can be very large and overflows
                    term = numerator / denominator;
                    s = s + term;
                    s = s - (int)s;
                    k++;
                }
            }
            //Right Portion
            for (k = d; k <= d + 100; k++)
            {
                numerator = Math.Pow(16, d - k);
                denominator = 8d * k + j;
                term = numerator / denominator;
                if (term < 1e-17)
                {
                    break;
                }
                s = s + term;
                s = s - (int)s;
            }
            return s;
        }
        public static (double value, string hex) CalcPi(int place)
        {
            var value = BbpfCalcInternal(place);
            return (value, ToHex(value));
        }
        private static string ToHex(double input) //Note the pointer '*out' points to hexOutput[] (char[]) in main(). Not possible to return char[] in C/C++
        {
            var hexNumbers = "0123456789ABCDEF";
            var rv = "0000000000000".ToArray();

            for (int i = 0; i <= 12; i++)
            {
                input = 16d * (input - (int)input);
                rv[i] = hexNumbers[(int)input];
            }
            return new string(rv);
        }
        private static double BbpfCalcInternal(int place)
        {
            using (var context = new Context())
            {
                var acceleratorId = Accelerator.Accelerators[1];
                using (var accelerator = Accelerator.Create(context, acceleratorId))
                {
                    var bbpf16jsdKernel = accelerator.LoadAutoGroupedStreamKernel<Index1, ArrayView<int>, ArrayView<int>, ArrayView<int>, ArrayView<double>>(LeftPortionThreaded);

                    Console.WriteLine($"Performing operations on {accelerator}, detected {accelerator.MaxNumThreads} Threads");
                    double result;
                    double p1 = 4d * Bbpf16jsd(accelerator, bbpf16jsdKernel, 1, place);
                    double p2 = 2d * Bbpf16jsd(accelerator, bbpf16jsdKernel, 4, place);
                    double p3 = Bbpf16jsd(accelerator, bbpf16jsdKernel, 5, place);
                    double p4 = Bbpf16jsd(accelerator, bbpf16jsdKernel, 6, place);

                    result = p1 - p2 - p3 - p4;
                    Console.WriteLine($"Debug: p1 = {p1}, p2 = {p2}, p3 = {p3}, p4 = {p4}, result = {result}");
                    return result - ((int)result) + 1d;
                }
            }
        }
    }
}
