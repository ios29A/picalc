﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace BbpSharp
{
    public class BbpSharp
    {
        private static double ExpoMod(int n, double k)
        {
            int bitsneeded = 0;
            for (; (1 << bitsneeded) < n; bitsneeded++) ;
            if ((1 << bitsneeded) != n) //because the increment is applied before the condition check, we need to decrement by one, unless the item was equal to n
            {
                bitsneeded--;
            }
            //First set t to be the largest power of two such that t ≤ n, and set r = 1.
            int t = 1 << bitsneeded;
            double r = 1;

            //Loop by the number of Binary positions
            for (int i = 0; i <= bitsneeded; i++)
            {
                if (n >= t) // if n ≥ t then
                {
                    r *= 16.0;// r ← br
                    r -= ((int)(r / k)) * k;// r ← r mod k, this operation is much faster than ordinaty Modulus %
                    n -= t;// n ← n − t
                }
                t >>= 1;// t ← t/2
                if (t >= 1) // if t ≥ 1 then
                {
                    r *= r;// r ← r^2
                    r -= ((int)(r / k)) * k;// r ← r mod k, this operation is much faster than ordinaty Modulus %
                }
            }
            return r;
        }

        //Left Portion for one thread - just does one term
        private static double LeftPortionThreaded(int k, int j, int d)
        {
            int kinit = k;
            int termsNo = 0;//Always go from 0-100000 in the terms[] DONT use K!
            double s = 0;
            for (; k < (kinit + 100000); k++)
            {
                double numerator, denominator;
                denominator = 8d * k + j;
                numerator = ExpoMod(d - k, denominator); // Binary algorithm for exponentiation with Modulo must be used becuase otherwise 16^(d-k) can be very large and overflows
                s += numerator / denominator;
                s -= (int)s;
                termsNo++;
            }
            return s;
        }

        //Bailey–Borwein–Plouffe Formula 16^d x Sj
        private static double Bbpf16jsd(int j, int d)
        {
            double s = 0;
            double numerator, denominator;
            double term;
            var noOfThreads = Environment.ProcessorCount;
            var tasksArray = new Task<double>[noOfThreads];
            int k = 0;
            while (k < d)
            {
                if (k + (100000 * noOfThreads) < d)//Only make threads for k up to less than d
                {
                    for (int i1 = 0; i1 < noOfThreads; i1++) //create and execute (noOfThreads) threads
                    {
                        var kOrigin = k;
                        tasksArray[i1] = Task.Run(() => LeftPortionThreaded(kOrigin, j, d));
                        k += 100000; //We need to run 100000 result on each thread because the thread overhead is much to great to run just 1
                    }
                    Task.WaitAll(tasksArray);
                    for (int i2 = noOfThreads - 1; i2 > -1; i2--) //fetch results from all threads - count backwards because last thread executed will be slowest (higher numbers)
                    {
                        s += tasksArray[i2].Result;
                        s -= (int)s;
                    }
                }
                else //if we are almost done and k + noOfThreads > d then do the last few terms single threaded
                {
                    denominator = 8d * k + j;
                    numerator = ExpoMod(d - k, denominator); // Binary algorithm for exponentiation with Modulo must be used becuase otherwise 16^(d-k) can be very large and overflows
                    term = numerator / denominator;
                    s += term;
                    s -= (int)s;
                    k++;
                }
            }
            //Right Portion
            for (k = d; k <= d + 100; k++)
            {
                numerator = Math.Pow(16, d - k);
                denominator = 8d * k + j;
                term = numerator / denominator;
                if (term < 1e-17) 
                {
                    break;
                }
                s += term;
                s -= (int)s;
            }
            return s;
        }

        //Bailey–Borwein–Plouffe Formula Calculation
        private static double BbpfCalc(int place)
        {
            double result;
            double p1 = (4d * Bbpf16jsd(1, place));
            double p2 = (2d * Bbpf16jsd(4, place));
            double p3 = Bbpf16jsd(5, place);
            double p4 = Bbpf16jsd(6, place);
            result = p1 - p2 - p3 - p4; 
            Console.WriteLine($"Debug: p1 = {p1}, p2 = {p2}, p3 = {p3}, p4 = {p4}");
            return result - ((int)result) + 1d;
        }

        private static string ToHex(double input)
        {
            var hexNumbers = "0123456789ABCDEF";
            var rv = "0000000000000".ToArray();

            for (int i = 0; i <= 12; i++)
            {
                input = 16d * (input - (int)input);
                rv[i] = hexNumbers[(int)input];
            }
            return new string(rv);
        }

        public static (double value, string hex) CalcPi(int place)
        {
            var value = BbpfCalc(place);
            return ((double)value, ToHex(value));
        }
    }

}
