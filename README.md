## PiCalc is a benchmarking programs for Windows, which calculates Pi digis by your processor ##

**How to do the test:**

* Clone repository to some folder
* Build _PiCalc.sln_ file with VS 2019 Community Edition

You will find the next executables:

* .\x64\Release\PiCalcCpp.exe - raw Pi calculation using native C++
* .\x64\Release\BbpCpp.exe - Bailey–Borwein–Plouffe formula implementation by native C++ (this is a clone of https://github.com/JMadgwick/bbpi)
* .\Release\net5.0\PiCalcSharp.exe - raw Pi calculation using .NET Core
* .\Release\net5.0\BbpSharp.exe - Bailey–Borwein–Plouffe formula implementation by .NET Core for parrallel hex calculations
* .\Release\net5.0\BbpSharp2XPrecision.exe - Bailey–Borwein–Plouffe formula implementation by .NET Core for parrallel hex calculations and 128 bits precision (more than 11'865'077 digits)
* .\Release\net5.0\BbpSharpGPU.exe - Bailey–Borwein–Plouffe formula implementation by .NET Core for parrallel hex calculations in OpenCL
* .\Release\net5.0\BbpSharpGpuCuda.exe - Bailey–Borwein–Plouffe formula implementation by .NET Core for parrallel hex calculations in NVidia CUDA


Run them with a parameter of number of digits like

PiCalcSharp.exe 10000

or

BbpSharp.exe 1000000

Btw. BBP will find only 13 hex digits of the Pi number, except for BbpSharp2XPrecision (25 digits)