﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PiCalcSharp
{
    public class PiCalc
    {
        public static string GetDigits(int digits)
        {
            digits++;

            var x = new uint[digits * 10 / 3 + 2];
            var r = new uint[digits * 10 / 3 + 2];

            var pi = new uint[digits];

            for (var j = 0; j < x.Length; j++)
                x[j] = 20;

            for (var i = 0; i < digits; i++)
            {
                var carry = 0u;
                for (var j = 0; j < x.Length; j++)
                {
                    var num = (uint)(x.Length - j - 1);
                    var dem = num * 2 + 1;

                    x[j] += carry;

                    var q = x[j] / dem;
                    r[j] = x[j] % dem;

                    carry = q * num;
                }

                pi[i] = x[x.Length - 1] / 10;

                r[x.Length - 1] = x[x.Length - 1] % 10;

                for (var j = 0; j < x.Length; j++)
                    x[j] = r[j] * 10;
            }
            var result = new char[digits];
            var c = 0u;
            for (var i = pi.Length - 1; i >= 0; i--)
            {
                pi[i] += c;
                c = pi[i] / 10;
                var d = (char)((pi[i] % 10) + '0');
                result[i] = d;
            }
            return new string(result);
        }
    }
}
