﻿using System;
using System.Diagnostics;

namespace BbpSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            var place = 0;
            if (args.Length > 0)
            {
                place = int.Parse(args[0].Replace("'", ""));
            }
            Console.WriteLine("Bailey-Borwein-Plouffe Formula for Pi");
            Console.WriteLine($"Detected {Environment.ProcessorCount} processors");

            var watch = Stopwatch.StartNew();
            var (value, hex) = BbpSharp.CalcPi(place);
            watch.Stop();

            Console.WriteLine($"Pi Estimation Decimal: {value}");
            Console.WriteLine($"Pi Estimation Hex: {hex}");
            Console.WriteLine($"Duration: {watch.Elapsed.TotalSeconds}s");
            return;
        }
    }
}
