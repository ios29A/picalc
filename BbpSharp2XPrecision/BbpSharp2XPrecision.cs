﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace BbpSharp
{
    public class BbpSharp2XPrecision
    {
        private static decimal ExpoMod(int n, decimal k)
        {
            int bitsneeded = 0;
            for (; (1 << bitsneeded) < n; bitsneeded++) ;
            if ((1 << bitsneeded) != n) //because the increment is applied before the condition check, we need to decrement by one, unless the item was equal to n
            {
                bitsneeded--;
            }
            //First set t to be the largest power of two such that t ≤ n, and set r = 1.
            int t = 1 << bitsneeded;
            decimal r = 1;

            //Loop by the number of Binary positions
            for (int i = 0; i <= bitsneeded; i++)
            {
                if (n >= t) // if n ≥ t then
                {
                    r *= 16;// r ← br
                    r%= k;// r ← r mod k
                    n -= t;// n ← n − t
                }
                t >>= 1;// t ← t/2
                if (t >= 1) // if t ≥ 1 then
                {
                    r*= r;// r ← r^2
                    r%= k;// r ← r mod k
                }

            }
            return r;
        }

        //Left Portion for one thread - just does one term
        private static decimal LeftPortionThreaded(int k, int j, int d)
        {
            int kinit = k;
            int termsNo = 0;//Always go from 0-100000 in the terms[] DONT use K!
            decimal s = 0;
            for (; k < (kinit + 100000); k++)
            {
                decimal numerator, denominator;
                denominator = 8 * k + j;
                numerator = ExpoMod(d - k, denominator); // Binary algorithm for exponentiation with Modulo must be used becuase otherwise 16^(d-k) can be very large and overflows
                s += numerator / denominator;
                s -= (int)s;
                termsNo++;
            }
            return s;
        }

        //Bailey–Borwein–Plouffe Formula 16^d x Sj
        private static decimal Bbpf16jsd(int j, int d)
        {

            decimal s = 0;
            decimal numerator, denominator;
            decimal term;
            var noOfThreads = Environment.ProcessorCount;
            var tasksArray = new Task<decimal>[noOfThreads];
            int k = 0;
            while (k < d)
            {
                if (k + (100000 * noOfThreads) < d)//Only make threads for k up to less than d
                {
                    for (int i1 = 0; i1 < noOfThreads; i1++) //create and execute (noOfThreads) threads
                    {
                        var kOrigin = k;
                        tasksArray[i1] = Task.Run(() => LeftPortionThreaded(kOrigin, j, d));
                        k += 100000; //We need to run 100000 result on each thread because the thread overhead is much to great to run just 1
                    }
                    Task.WaitAll(tasksArray);
                    s = tasksArray.Aggregate(s, (sum, t) => sum + t.Result - (int) (sum + t.Result));
                }
                else //if we are almost done and k + noOfThreads > d then do the last few terms single threaded
                {
                    denominator = 8 * k + j;
                    numerator = ExpoMod(d - k, denominator); // Binary algorithm for exponentiation with Modulo must be used becuase otherwise 16^(d-k) can be very large and overflows
                    term = numerator / denominator;
                    s += term;
                    s -= (int)s;
                    k++;
                }
            }
            //Right Portion
            for (k = d; k <= d + 100; k++)
            {
                numerator = (decimal) Math.Pow(16,  (d - k));
                denominator = 8 * k + j;
                term = numerator / denominator;
                if (term < 1e-17M) 
                {
                    break;
                }
                s += term;
                s -= (int)s;
            }
            return s;
        }

        //Bailey–Borwein–Plouffe Formula Calculation
        private static decimal BbpfCalc(int place)
        {
            decimal result;
            decimal p1 = (4 * Bbpf16jsd(1, place));
            decimal p2 = (2 * Bbpf16jsd(4, place));
            decimal p3 = Bbpf16jsd(5, place);
            decimal p4 = Bbpf16jsd(6, place);
            result = p1 - p2 - p3 - p4; 
            Console.WriteLine($"Debug: p1 = {p1}, p2 = {p2}, p3 = {p3}, p4 = {p4}");
            return result - ((int)result) + 1;
        }

        private static string ToHex(decimal input)
        {
            var hexNumbers = "0123456789ABCDEF";
            var rv = "00000000000000000000000000".ToArray();

            for (int i = 0; i <= 24; i++)
            {
                input = 16 * (input - (int)input);
                rv[i] = hexNumbers[(int)input];
            }
            return new string(rv);
        }

        public static (double value, string hex) CalcPi(int place)
        {
            var value = BbpfCalc(place);
            return ((double) value, ToHex(value));
        }
    }

}
