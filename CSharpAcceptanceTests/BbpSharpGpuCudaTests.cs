﻿using System;
using CSharpAcceptanceTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;



namespace CSharpAcceptanceTests
{
    /// <summary>
    /// OpenCL tests
    /// </summary>
    [TestClass]
    public class BbpSharpGpuCudaTests : BbpTestsBase
    {
        protected override Func<int, (double, string)> WorkerDelegate => BbpSharp.BbpSharpGpuCuda.CalcPi;
        protected override int ValuableDigits { get; set; } = 4;

        /// <summary>
        /// This tests OpenCL implementation of the Bbp alg.
        /// On NVidia GTX 1060 I had only 4 correct digits of the Pi number for specific position, because of low precision (float),
        /// even in the code it's double
        /// </summary>
        [TestMethod]
        public void BbpSharpGpuCudaTest100000_10_ValuableDigits4()
        {
            var hex = CalcBppHex(100_000, 10);

            Assert.AreEqual(Resources.Pi100000_10_hex, hex, "Seems you have a low GPU precision, see the function description");
        }

        /// <summary>
        /// This tests OpenCL implementation of the Bbp alg.
        /// On NVidia GTX 1060 I had only 4 correct digits of the Pi number for specific positon, because of low precision (float),
        /// even in the code it's double, but this test will fail if your GPU has more precision
        /// </summary>
        [TestMethod]
        public void BbpSharpGpuCudaTest100000_10_ValuableDigits5()
        {
            ValuableDigits = 5;

            var hex = CalcBppHex(100_000, 10);

            Assert.AreNotEqual(Resources.Pi100000_10_hex, hex, "Seems you have a better GPU precision, see the function description");
        }
    }
}
