﻿using Cloo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace BbpSharp
{
    public class BbpSharpGpuOpenCL
    {
        private static int[] powerTable = Enumerable.Range(0, 64).Select(i => 1 << i).ToArray();

        private static double BbpfCalcInternal(ComputeContext context, int place)
        {
            var devs = new List<ComputeDevice>();
            devs.Add(ComputePlatform.Platforms[0].Devices[0]);

            string leftPortionThreaded = @"
            double ExpoMod(int n, double k, __global int *pwrtbl)
            {
  	            int bitsneeded = 0;//The number of binary positions and the location in the table of the highest needed power
	            for (; pwrtbl[bitsneeded] < n; bitsneeded++);//Move through table to find position of the largest power of two < n
	            if (pwrtbl[bitsneeded] != n && n > 0) //because the increment is applied before the condition check, we need to decrement by one, unless the item was equal to n
                {
                    bitsneeded--;
                }

	            //First set t to be the largest power of two such that t ≤ n, and set r = 1.
	            int t = pwrtbl[bitsneeded];
	            double r = 1;

	            //Loop by the number of Binary positions
	            for (int i = 0; i <= bitsneeded; i++)
	            {
		            if (n >= t) // if n ≥ t then
		            {
			            r*= 16.0;// r ← br
                        r-= ((int) (r / k)) * k;// r ← r mod k
			            n-= t;// n ← n − t
		            }
		            t/= 2;// t ← t/2
		            if (t >= 1) // if t ≥ 1 then
		            {
			            r*= r;// r ← r^2
			            r-= ((int) (r / k)) * k;// r ← r mod k
		            }
	            }
	            return r;
            }
            __kernel void LeftPortionThreaded(__global int *kPointer, __global int *jPointer, __global int *dPointer, __global int *powerOf2, __global double *res)
            {
                int i = get_global_id(0);
                int k = kPointer[i], j = jPointer[i], d = dPointer[i];
                int kInit = k;
                int termsNo = 0;//Always go from 0-100000 in the terms[] DONT use K!
                double s = 0;
                for (; k < (kInit + 100000); k++)
                {
                    double numerator = 10, denominator;
                    denominator = 8. * k + j;
                    numerator = ExpoMod(d - k, denominator, powerOf2); // Binary algorithm for exponentiation with Modulo must be used becuase otherwise 16^(d-k) can be very large and overflows
                    s+= numerator / denominator;
                    s-= (int)s;
                    termsNo++;
                }
                res[i] = s;
            };";

            ComputeProgram prog;
            try
            {

                prog = new ComputeProgram(context, leftPortionThreaded);
                prog.Build(devs, "", null, IntPtr.Zero);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
            ComputeKernel kernelLeftPortionThreaded = prog.CreateKernel("LeftPortionThreaded");
            double result;
            double p1 = 4d * Bbpf16jsd(context, kernelLeftPortionThreaded, 1, place);
            double p2 = 2d * Bbpf16jsd(context, kernelLeftPortionThreaded, 4, place);
            double p3 = Bbpf16jsd(context, kernelLeftPortionThreaded, 5, place);
            double p4 = Bbpf16jsd(context, kernelLeftPortionThreaded, 6, place);
            result = p1 - p2 - p3 - p4;
            Console.WriteLine($"Debug: p1 = {p1}, p2 = {p2}, p3 = {p3}, p4 = {p4}");
            return result - ((int)result) + 1d;
        }
        private static string ToHex(double input) //Note the pointer '*out' points to hexOutput[] (char[]) in main(). Not possible to return char[] in C/C++
        {
            var hexNumbers = "0123456789ABCDEF";
            var rv = "0000000000000".ToArray();

            for (int i = 0; i <= 12; i++)
            {
                input = 16d * (input - (int)input);
                rv[i] = hexNumbers[(int)input];
            }
            return new string(rv);
        }


        public static (double value, string hex) CalcPi(int place)
        {
            ComputeContextPropertyList properties = new ComputeContextPropertyList(ComputePlatform.Platforms[0]);
            ComputeContext context = new ComputeContext(ComputeDeviceTypes.All, properties, null, IntPtr.Zero);
            var value = BbpfCalcInternal(context, place);
            return (value, ToHex(value));
        }

        private struct GPUTask
        {
            public int k;
            public int j;
            public int d;
        }

        //Bailey–Borwein–Plouffe Formula 16^d x Sj
        private static double Bbpf16jsd(ComputeContext context, ComputeKernel kernelLeftPortionThreaded, int j, int d)
        {
            double s = 0;
            double numerator, denominator;
            double term;
            int noOfThreads = (int)ComputePlatform.Platforms[0].Devices[0].MaxWorkGroupSize;
            var listOfGPUTasks = new List<GPUTask>(noOfThreads);
            int k = 0;
            while (k < d)
            {
                if (k + (noOfThreads) < d)//Only make threads for k up to less than d
                {
                    listOfGPUTasks.Clear();
                    for (int i1 = 0; i1 < noOfThreads; i1++) //create and execute (noOfThreads) threads
                    {
                        listOfGPUTasks.Add(new GPUTask() { k = k, j = j, d = d });
                        k = k + 100000; //We need to run 100000 result on each thread because the thread overhead is much to great to run just 1
                    }

                    var kArray = listOfGPUTasks.Select(t => t.k).ToArray();
                    var jArray = listOfGPUTasks.Select(t => t.j).ToArray();
                    var dArray = listOfGPUTasks.Select(t => t.d).ToArray();
                    var resArray = new double[listOfGPUTasks.Count];
                    // k == 0, j == 1, d == 400'000, s = 0.13406411870842644
                    var kBuffer = new ComputeBuffer<int>(context, ComputeMemoryFlags.ReadWrite | ComputeMemoryFlags.UseHostPointer, kArray);
                    var jBuffer = new ComputeBuffer<int>(context, ComputeMemoryFlags.ReadWrite | ComputeMemoryFlags.UseHostPointer, jArray);
                    var dBuffer = new ComputeBuffer<int>(context, ComputeMemoryFlags.ReadWrite | ComputeMemoryFlags.UseHostPointer, dArray);
                    var resBuffer = new ComputeBuffer<double>(context, ComputeMemoryFlags.ReadWrite | ComputeMemoryFlags.UseHostPointer, resArray);
                    var powerOf2Buffer = new ComputeBuffer<int>(context, ComputeMemoryFlags.ReadWrite | ComputeMemoryFlags.UseHostPointer, powerTable);

                    kernelLeftPortionThreaded.SetMemoryArgument(0, kBuffer);
                    kernelLeftPortionThreaded.SetMemoryArgument(1, jBuffer);
                    kernelLeftPortionThreaded.SetMemoryArgument(2, dBuffer);
                    kernelLeftPortionThreaded.SetMemoryArgument(3, powerOf2Buffer);
                    kernelLeftPortionThreaded.SetMemoryArgument(4, resBuffer);

                    ComputeCommandQueue queue = new ComputeCommandQueue(context, ComputePlatform.Platforms[0].Devices[0], ComputeCommandQueueFlags.None);
                    queue.Execute(kernelLeftPortionThreaded, null, new long[] { listOfGPUTasks.Count }, null, null);

                    var arrC = new double[listOfGPUTasks.Count];
                    GCHandle arrCHandle = GCHandle.Alloc(arrC, GCHandleType.Pinned);
                    queue.Read(resBuffer, true, 0, listOfGPUTasks.Count, arrCHandle.AddrOfPinnedObject(), null);

                    for (int i2 = noOfThreads - 1; i2 > -1; i2--) //fetch results from all threads - count backwards because last thread executed will be slowest (higher numbers)
                    {
                        s = s + arrC[i2];
                        s = s - (int)s;
                    }
                }
                else //if we are almost done and k + noOfThreads > d then do the last few terms single threaded
                {
                    denominator = 8d * k + j;
                    numerator = ExpoMod(d - k, denominator); // Binary algorithm for exponentiation with Modulo must be used becuase otherwise 16^(d-k) can be very large and overflows
                    term = numerator / denominator;
                    s = s + term;
                    s = s - (int)s;
                    k++;
                }
            }
            //Right Portion
            for (k = d; k <= d + 100; k++)
            {
                numerator = Math.Pow(16, d - k);
                denominator = 8d * k + j;
                term = numerator / denominator;
                if (term < 1e-17)
                {
                    break;
                }
                s = s + term;
                s = s - (int)s;
            }
            return s;
        }

        private static double ExpoMod(int n, double k)
        {
            int bitsneeded = 0;//The number of binary positions and the location in the table of the highest needed power
            for (; powerTable[bitsneeded] < n; bitsneeded++) ;//Move through table to find position of the largest power of two < n
            if (powerTable[bitsneeded] != n && n > 0) //because the increment is applied before the condition check, we need to decrement by one, unless the item was equal to n
            {
                bitsneeded--;
            }
            //First set t to be the largest power of two such that t ≤ n, and set r = 1.
            int t = powerTable[bitsneeded];
            double r = 1;

            //Loop by the number of Binary positions
            for (int i = 0; i <= bitsneeded; i++)
            {
                if (n >= t) // if n ≥ t then
                {
                    r = r * 16.0;// r ← br
                    r = r - ((int)(r / k)) * k;// r ← r mod k
                    n = n - t;// n ← n − t
                }
                t = t / 2;// t ← t/2
                if (t >= 1) // if t ≥ 1 then
                {
                    r = r * r;// r ← r^2
                    r = r - ((int)(r / k)) * k;// r ← r mod k
                }
            }
            return r;
        }
    }
}
