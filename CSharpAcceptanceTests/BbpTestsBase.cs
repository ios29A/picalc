﻿using System;
using System.Linq;


namespace CSharpAcceptanceTests
{
    public abstract class BbpTestsBase
    {
        /// <summary>
        /// Pointer to BbpCalc function
        /// </summary>
        protected abstract Func<int, (double value, string hex)> WorkerDelegate { get; }

        /// <summary>
        /// Valueab
        /// </summary>
        protected abstract int ValuableDigits { get; set; }
        
        protected string CalcBppHex(int sourcePosition, int count)
        {
            return string.Join("",
                    Enumerable.Range(0, (int)Math.Ceiling((double)count / ValuableDigits))
                        .Select(batch => WorkerDelegate(sourcePosition + batch * ValuableDigits).hex.Substring(0, ValuableDigits)))
                .Substring(0, count);
        }
    }
}
