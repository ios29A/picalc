﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using ILGPU;
using ILGPU.Runtime;

namespace BbpSharpGpuCuda
{
    class Program
    {

        static void Main(string[] args)
        {
            var place = 0;
            if (args.Length > 0)
            {
                place = int.Parse(args[0].Replace("'", ""));
            }
            Console.WriteLine("Bailey-Borwein-Plouffe Formula for Pi");

            var watch = Stopwatch.StartNew();
            var (value, hex) = BbpSharp.BbpSharpGpuCuda.CalcPi(place);
            watch.Stop();

            Console.WriteLine($"Pi Estimation Decimal: {value}");
            Console.WriteLine($"Pi Estimation Hex: {hex}");
            Console.WriteLine($"Duration: {watch.Elapsed.TotalSeconds}s");
        }
    }
}
